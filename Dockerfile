FROM node:8.0-alpine
RUN mkdir -p /home/app
COPY ./app /home/app
WORKDIR /home/app
EXPOSE 8801
CMD ["node", "app.js"]